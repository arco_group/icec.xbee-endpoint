// -*- mode: c++; coding: utf-8 -*-

module Example {
    interface Hello {
	void sayHello(string name);
    };
};
